package com.example.demo.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Task;
import com.example.demo.service.TaskService;

@Controller
public class TaskController {
	public static final String TASK_BLANK = "タスクを入力してください";
	public static final String LIMIT_BLANK = "期限を設定してください";
	public static final String CONTENT_OVER ="140文字以内で入力してください";
	public static final String INVALID_DATE ="無効な日付です";
	public static final String INVALID_PARAMETER ="不正なパラメータです";
	@Autowired
	TaskService taskService;

	//画面表示
	@GetMapping
	public ModelAndView top() {
	ModelAndView mav = new ModelAndView();
	//取得
	Date today = new Date();
	List<Task> taskDate = taskService.findAllTask();

    // Date型の日時をCalendar型に変換
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(today);
    // 日時を加算する
    calendar.add(Calendar.DATE, 1);
    // Calendar型の日時をDate型に戻す
    Date d1 = calendar.getTime();
	//画面推移先
	mav.setViewName("/top");
	//オブジェクト保管
	mav.addObject("tasks",taskDate);
	mav.addObject("day",d1);
	return mav;
	}

	//削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		taskService.deleteByIdTask(id);
		return new ModelAndView("redirect:/");
	}

	//タスク変更画面
	@GetMapping("/EditTasks/{id}")
	public ModelAndView editContent(@PathVariable String id) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();

		if (!id.matches("^[0-9]+$") || (id == null)){
			errorMessages.add(INVALID_PARAMETER);
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("/top");
			List<Task> taskDate = taskService.findAllTask();
			mav.addObject("tasks",taskDate);
			return mav;
		}

		Integer intId = Integer.parseInt(id);
		Task task  = taskService.editTask(intId);

		 if (task == null){
			errorMessages.add(INVALID_PARAMETER);
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("/top");
			List<Task> taskDate = taskService.findAllTask();
			mav.addObject("tasks",taskDate);
			return mav;
		}
		mav.addObject("formModel", task);
		mav.setViewName("/EditTasks");
		return mav;
	}

	//タスク編集処理
	@PutMapping("/edit/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel")
	Task task,@RequestParam(name = "limit") String limit) {
		Date date = new Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		List<String> errorMessages = new ArrayList<>();

		task.setId(id); // UrlParameterのidを更新するentityにセット
		Task taskDate = taskService.editTask(id);
		task.setCreated_date(taskDate.getCreated_date());
		task.setUpdated_date(date);

		//contentのチェック
		if((!StringUtils.hasLength(task.getContent())) || (!StringUtils.hasText(task.getContent()))) {
			errorMessages.add(TASK_BLANK);
			} else if(140 < task.getContent().length()) {
				errorMessages.add(CONTENT_OVER);
		}
		//limitのチェック
		if((!StringUtils.hasLength(limit)) & (!StringUtils.hasText(limit))) {
			errorMessages.add(LIMIT_BLANK);
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date limitDate = null;
		limit += " 23:59:59";
		try {
			limitDate = df.parse(limit);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if(limitDate.before(timestamp)) {
			errorMessages.add(INVALID_DATE);
		}

		if(errorMessages.size() != 0) {
			ModelAndView mav = new ModelAndView();
			List<Task> taskFindDate = taskService.findAllTask();
			mav.setViewName("/top");
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("tasks",taskFindDate);
			return mav;
		}
		task.setLimit_date(limitDate);

		taskService.saveTask(task); // 編集した投稿を更新
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	//追加画面初期表示
	@GetMapping("/AddTasks")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Task task = new Task();
		// 画面遷移先を指定
		mav.setViewName("/AddTasks");
		// 準備した空のentityを保管
		mav.addObject("formModel", task);
		return mav;
	}

	//追加処理
	@PostMapping("/add")
	public ModelAndView addTask(@ModelAttribute("formModel") Task task,
			@RequestParam(name = "limited_date") String limit) {
		Date date = new Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		List<String> errorMessages = new ArrayList<>();

		//contentのチェック
		if((!StringUtils.hasLength(task.getContent())) || (!StringUtils.hasText(task.getContent()))) {
			errorMessages.add(TASK_BLANK);
		} else if(140 < task.getContent().length()) {
			errorMessages.add(CONTENT_OVER);
		}
		task.setCreated_date(date);
		task.setUpdated_date(date);
		//limit_dateのチェック
		if((!StringUtils.hasLength(limit)) & (!StringUtils.hasText(limit))) {
			errorMessages.add(LIMIT_BLANK);
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date limitDate = null;
		limit += " 23:59:59";

		try {
			limitDate = df.parse(limit);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if(limitDate.before(timestamp)) {
			errorMessages.add(INVALID_DATE);
		}

		if(errorMessages.size() != 0) {
			ModelAndView mav = new ModelAndView();
			List<Task> taskDate = taskService.findAllTask();
			mav.setViewName("/top");
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("tasks",taskDate);
			return mav;
		}
		task.setLimit_date(limitDate);
		//テーブルに格納
		taskService.saveTask(task);
		return new ModelAndView("redirect:/");

	}

	//絞り込み機能
	@GetMapping("/search")
	public ModelAndView date(@RequestParam(name = "start") String start,
			@RequestParam(name = "end") String end) {
		ModelAndView mav = new ModelAndView();
		// 投稿を全件取得
		List<Task> taskData = taskService.searchTask(start, end);
		Date today = new Date();
		// Date型の日時をCalendar型に変換
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(today);
	    // 日時を加算する
	    calendar.add(Calendar.DATE, 1);
	    // Calendar型の日時をDate型に戻す
	    Date d1 = calendar.getTime();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("tasks", taskData);
		mav.addObject("day",d1);
		// 日付start
		mav.addObject("start", start);
		// 日付end
		mav.addObject("end", end);
		return mav;
	}




}