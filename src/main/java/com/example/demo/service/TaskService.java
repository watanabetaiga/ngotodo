package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Task;
import com.example.demo.repository.TaskRepository;

@Service
@Transactional
public class TaskService {

	@Autowired
	TaskRepository taskRepository;

	// レコード全件取得
	public List<Task> findAllTask() {
		return taskRepository.findAll();
	}

	// レコード追加
	public void saveTask(Task task) {
		taskRepository.save(task);
	}

	// レコード削除
	public void deleteByIdTask(Integer id) {
		taskRepository.deleteById(id);
	}

	// レコード1件取得
	public Task editTask(Integer id) {
		Task task = (Task) taskRepository.findById(id).orElse(null);
		return task;
	}

	// 絞り込み
	public List<Task> searchTask(String start, String end) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(start != "") {
			start += " 00:00:00";
		} else {
			start = "2022-01-01 00:00:00";
		}

		if(end != "") {
			end += " 23:59:59";
		} else {
			Date now = new Date();
			end = df.format(now);
		}

		Date startDate = null;
		try {
			startDate = df.parse(start);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Date endDate = null;
		try {
			endDate = df.parse(end);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return taskRepository.findByLimitDateBetween(startDate, endDate);
	}
}