package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tasks")
public class Task {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; // タスクID

	@Column
	private String content; // タスク内容

	@Column
	private int status; // タスクステータス

	@Column
	private Date limit_date; // タスク期限

	@Column
	private Date created_date; // 登録日時

	@Column
	private Date updated_date; // 更新日時

	// 返信IDを取得
	public int getId() {
		return id;
	}

	// 返信IDを格納
	public void setId(int id) {
		this.id = id;
	}

	// 返信内容を取得
	public String getContent() {
		return content;
	}

	// 返信内容を格納
	public void setContent(String content) {
		this.content = content;
	}

	// タスクステータスを取得
	public int getStatus() {
		return status;
	}

	// タスクステータスを格納
	public void setStatus(int status) {
		this.status = status;
	}

	// タスク期限を取得
	public Date getLimit_date() {
		return limit_date;
	}

	// タスク期限を格納
	public void setLimit_date(Date limit_date) {
		this.limit_date = limit_date;
	}

	// タスク登録日時を取得
	public Date getCreated_date() {
		return created_date;
	}

	// タスク登録日時を格納
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	// タスク更新時間を取得
	public Date getUpdated_date() {
		return updated_date;
	}

	// タスク更新時間を格納
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
}