package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {
	@Query("select tasks from Task tasks where tasks.created_date between :startDate and :endDate " + "ORDER BY tasks.created_date DESC")
	List<Task> findByLimitDateBetween(Date startDate, Date endDate);
	@Query("select tasks from Task tasks ORDER BY limit_date DESC")
	List<Task> findAll();
}